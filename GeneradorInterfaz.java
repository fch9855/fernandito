/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EscenaJuego;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;

/**
 *
 * @author Fernando Changoluisa
 */
public class GeneradorInterfaz {
    private BorderPane root;
    Image fondo = new Image(Constantes.getRuta()+"fondo.gif");
    BackgroundSize bsize = new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false,false,true , false);
    
    public GeneradorInterfaz(){
        root = new BorderPane();
        root.setBackground(new Background(new BackgroundImage(fondo,BackgroundRepeat.NO_REPEAT,BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER ,bsize)));
        root.setCenter(Centro());
        root.setBottom(Fondo());
        
        
    }
    
    
    public HBox Fondo (){
        HBox menu = new HBox();
        Button start = new Button("Start");
        Button score = new Button("Scores");
        menu.getChildren().addAll(start,score);
        menu.setSpacing(20);
        
        return menu;
        
    }
    
    
    public VBox Centro(){
        //try{
            VBox centro = new VBox();
            ImageView img = new ImageView ( new Image(Constantes.getRuta()+"prueba.png"));
            Label l1 = new Label("Ingresa tu nombre: ");
            TextField nombre = new TextField();
        //catch(){
            centro.getChildren().addAll(img, l1, nombre);
            
        return centro;
    }

    public BorderPane getRoot() {
        return root;
    }
    
    
   
    
}
